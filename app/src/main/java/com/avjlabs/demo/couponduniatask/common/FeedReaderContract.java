package com.avjlabs.demo.couponduniatask.common;

import android.content.Context;
import android.provider.BaseColumns;


public class FeedReaderContract {
    public FeedReaderContract() {
    }

    //inner class defines the table contents
    public static abstract class FeedEntry extends Context implements BaseColumns {
        public static final String DB_NAME = "task.db";

        public static final String DB_KEY_ID = "id";

        public static final String KEY_CD_LOG_TAG = "TASK";

        public static final String PROTOCOL = "http://";
        public static final String DOMAIN_NAME = "staging.couponapitest.com";
        public static final String URL_PATH_GET_TASK_DATA = PROTOCOL + DOMAIN_NAME + "/task_data.txt";

        public static final String TABLE_CACHE = "cache";
        public static final String DB_KEY_REQUEST = "request_name";
        public static final String DB_KEY_RESPONSE_DATA = "response_data";

        public static final String DB_KEY_CREATED = "created";
        public static final String DB_KEY_UPDATED = "updated";

        public static final String KEY_JSON_TAG_STATUS = "status";
        public static final String KEY_JSON_TAG_STATUS_RES_CODE = "rcode";
        public static final String KEY_JSON_TAG_DATA = "data";
        public static final String KEY_JSON_TAG_DATA_OBJECT_OUTLETNAME = "OutletName";
        public static final String KEY_JSON_TAG_DATA_OBJECT_LATITUDE = "Latitude";
        public static final String KEY_JSON_TAG_DATA_OBJECT_LONGITUDE = "Longitude";
        public static final String KEY_DATA_DISTANCE = "distance";
        public static final String KEY_JSON_TAG_DATA_OBJECT_IMAGE_URL = "LogoURL";
    }

}
