package com.avjlabs.demo.couponduniatask.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;


public class DatabaseHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = FeedReaderContract.FeedEntry.DB_NAME;


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        DB_Helper helper = new DB_Helper();
        helper.createCacheTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }


    public void addToCache(String requestData, JSONObject responseData) {

        String countQuery = "SELECT  * FROM " + FeedReaderContract.FeedEntry.TABLE_CACHE + " WHERE " + FeedReaderContract.FeedEntry.DB_KEY_REQUEST + " = '" + requestData + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        if(rowCount>0) {
            cursor.moveToFirst();
            db.close();
            updatetoCache(requestData,responseData);
        }else{

            ContentValues values = new ContentValues();
            values.put(FeedReaderContract.FeedEntry.DB_KEY_REQUEST, requestData);
            values.put(FeedReaderContract.FeedEntry.DB_KEY_RESPONSE_DATA, responseData.toString());

            db.insert(FeedReaderContract.FeedEntry.TABLE_CACHE, null, values);
            db.close();
        }

    }

    public void updatetoCache(String requestData, JSONObject responseData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(FeedReaderContract.FeedEntry.DB_KEY_RESPONSE_DATA, responseData.toString());
        // set the format to sql date time
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        values.put(FeedReaderContract.FeedEntry.DB_KEY_UPDATED, dateFormat.format(date));

        db.update(FeedReaderContract.FeedEntry.TABLE_CACHE, values, FeedReaderContract.FeedEntry.DB_KEY_REQUEST + " = '" + requestData + "' ", null);
        db.close(); // Closing database connection
    }


    public String fetchFromCache(String requestData) {
        String responseData = "";
        String countQuery = "SELECT  * FROM " + FeedReaderContract.FeedEntry.TABLE_CACHE + " WHERE " + FeedReaderContract.FeedEntry.DB_KEY_REQUEST + " = '" + requestData + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int rowCount = cursor.getCount();
        cursor.moveToFirst();
        if (rowCount > 0) {
            if (cursor.moveToFirst()) {
                responseData = cursor.getString(2);
            }

        }else{
            responseData="";
        }
        db.close();
        cursor.close();
        return responseData;
    }


}
