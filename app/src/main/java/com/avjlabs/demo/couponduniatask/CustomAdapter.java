package com.avjlabs.demo.couponduniatask;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avjlabs.demo.couponduniatask.common.FeedReaderContract;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by anaghvj on 07-07-2015.
 */
public class CustomAdapter extends BaseAdapter {
    Context mContext;
    private ArrayList<HashMap<String, String>> mData = new ArrayList<>();
    ImageView imageViewLogo;

    public CustomAdapter(Context context, ArrayList<HashMap<String, String>> restaurauntList) {
        this.mData = restaurauntList;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolderItem viewHolderItem;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, null);

            viewHolderItem = new ViewHolderItem();
            viewHolderItem.textViewName = (TextView) convertView.findViewById(R.id.textViewRName);
            viewHolderItem.textViewDistance = (TextView) convertView.findViewById(R.id.textViewdistance);
            viewHolderItem.imageViewLogo = (ImageView) convertView.findViewById(R.id.imageViewLogo);
viewHolderItem.position=position;
            //store holder with view
            convertView.setTag(viewHolderItem);
        } else {
            viewHolderItem = (ViewHolderItem) convertView.getTag();
        }

        viewHolderItem.textViewName.setText(mData.get(position).get(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_OUTLETNAME));
        double Kdistance, distanceFromRest;
        distanceFromRest = Double.valueOf(mData.get(position).get(FeedReaderContract.FeedEntry.KEY_DATA_DISTANCE));
        if (distanceFromRest > 1000) {
            Kdistance = distanceFromRest / 1000;
            //in Km.
            viewHolderItem.textViewDistance.setText(String.format("%.2f", Kdistance) + " km");

        } else {
            Kdistance = distanceFromRest;
            viewHolderItem.textViewDistance.setText(String.format("%.2f", Kdistance) + " m");
            //in Meters
        }
        String filepath = mData.get(position).get(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_IMAGE_URL);
        if (filepath == null || filepath.isEmpty() || filepath.equals("") || filepath.equals("null")) {
            //handle placeholder
        } else {
           // new ImageDownloader(viewHolderItem.imageViewLogo).execute(filepath);
           // new ImageDownloader(viewHolderItem,position).execute(filepath);
        }

        return convertView;
    }

    class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;
        int mPosition;
        ViewHolderItem mHolder;

        public ImageDownloader(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        public ImageDownloader(ViewHolderItem viewHolderItem, int position) {
            mHolder=viewHolderItem;
            mPosition=position;
        }

        protected Bitmap doInBackground(String... urls) {
            String url = urls[0];
            Bitmap mIcon = null;
            try {
                urls[0] = urls[0].replaceAll("\\\\", "");
                InputStream in = new java.net.URL(urls[0]).openStream();
                Log.e(FeedReaderContract.FeedEntry.KEY_CD_LOG_TAG, "img " + urls[0]);
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e(FeedReaderContract.FeedEntry.KEY_CD_LOG_TAG, "img " + e.toString());
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            if(mHolder.position==mPosition){
                  mHolder.imageViewLogo.setImageBitmap(result);
            }
        }
    }

    static class ViewHolderItem {
        TextView textViewName;
        TextView textViewDistance;
        ImageView imageViewLogo;
        int position;

    }

}
