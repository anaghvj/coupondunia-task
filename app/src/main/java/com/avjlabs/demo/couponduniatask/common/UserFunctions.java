package com.avjlabs.demo.couponduniatask.common;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class UserFunctions {

    private String URL = FeedReaderContract.FeedEntry.URL_PATH_GET_TASK_DATA;
    private JSONParser jsonParser;

    // constructor
    public UserFunctions() {
        jsonParser = new JSONParser();
    }

    public JSONObject getRestaurantDetails() {
        List<NameValuePair> params=new ArrayList();
        JSONObject json = jsonParser.makeHttpRequest(URL, "GET",params);
        return json;
    }


}
