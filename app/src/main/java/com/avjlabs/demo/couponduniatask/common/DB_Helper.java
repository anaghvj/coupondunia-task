package com.avjlabs.demo.couponduniatask.common;

import android.database.sqlite.SQLiteDatabase;


public class DB_Helper {

    public void createCacheTable(SQLiteDatabase checkDB1) {
        String CREATE_CACHE_TABLE = "CREATE TABLE if not exists " + FeedReaderContract.FeedEntry.TABLE_CACHE + " ("
                + FeedReaderContract.FeedEntry.DB_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + FeedReaderContract.FeedEntry.DB_KEY_REQUEST + " TEXT, "
                + FeedReaderContract.FeedEntry.DB_KEY_RESPONSE_DATA + " TEXT, "
                + FeedReaderContract.FeedEntry.DB_KEY_CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP, "
                + FeedReaderContract.FeedEntry.DB_KEY_UPDATED + " DATETIME "
                + "); ";

        checkDB1.execSQL(CREATE_CACHE_TABLE);

    }

}
