package com.avjlabs.demo.couponduniatask;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.avjlabs.demo.couponduniatask.common.DB_Helper;
import com.avjlabs.demo.couponduniatask.common.DatabaseHandler;
import com.avjlabs.demo.couponduniatask.common.FeedReaderContract;
import com.avjlabs.demo.couponduniatask.common.NetworkUtil;
import com.avjlabs.demo.couponduniatask.common.UserFunctions;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class RestaurantOffers extends ListActivity {
    ListView lv;
    CustomAdapter customAdapter;
    LocationListener locationListener;
    double uLat, uLon;
    ArrayList<HashMap<String, String>> restaurauntList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_offers);

        getLocation();
        lv = getListView();
        View emptyView = findViewById(R.id.emptyView);
        lv.setEmptyView(emptyView);
        new getData().execute();
        //customAdapter = new CustomAdapter(RestaurantOffers.this,list);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_restaurant_offers, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class getData extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            if (jsonObject != null) {
                if (jsonObject.has(FeedReaderContract.FeedEntry.KEY_JSON_TAG_STATUS)) {
                    try {
                        JSONObject statusJsonObject = jsonObject.getJSONObject(FeedReaderContract.FeedEntry.KEY_JSON_TAG_STATUS);
                        if (statusJsonObject.has(FeedReaderContract.FeedEntry.KEY_JSON_TAG_STATUS_RES_CODE)) {
                            int rcode = statusJsonObject.getInt(FeedReaderContract.FeedEntry.KEY_JSON_TAG_STATUS_RES_CODE);
                            if (rcode == 200) {
                                //OK
                                if (jsonObject.has(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA)) {
                                    DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                                    db.addToCache("task", jsonObject);
                                    JSONObject dataJsonObject1 = jsonObject.getJSONObject(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA);
                                    Iterator iterator = dataJsonObject1.keys();
                                    restaurauntList = new ArrayList<>();
                                    while (iterator.hasNext()) {
                                        HashMap<String, String> details = new HashMap<>();

                                        String name = (String) iterator.next();
                                        String RestName = dataJsonObject1.getJSONObject(name).getString(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_OUTLETNAME);
                                        String rlat = dataJsonObject1.getJSONObject(name).getString(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_LATITUDE);
                                        String rLon = dataJsonObject1.getJSONObject(name).getString(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_LONGITUDE);
                                        double distanceFromRest = calDistance(uLat, uLon, Double.valueOf(rlat), Double.valueOf(rLon));
                                        String ImageUrl = dataJsonObject1.getJSONObject(name).getString(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_IMAGE_URL);

                                        details.put(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_OUTLETNAME, RestName);
                                        details.put(FeedReaderContract.FeedEntry.KEY_DATA_DISTANCE, String.valueOf(distanceFromRest));
                                        details.put(FeedReaderContract.FeedEntry.KEY_JSON_TAG_DATA_OBJECT_IMAGE_URL, ImageUrl);

                                        restaurauntList.add(details);


                                    }
                                    //Collections.sort(restaurauntList,new MapComapare(FeedReaderContract.FeedEntry.KEY_DATA_DISTANCE));
                                    Collections.sort(restaurauntList, new Comparator<HashMap<String, String>>() {
                                        @Override
                                        public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
                                            return lhs.get(FeedReaderContract.FeedEntry.KEY_DATA_DISTANCE).compareTo(rhs.get(FeedReaderContract.FeedEntry.KEY_DATA_DISTANCE));
                                        }
                                    });
                                    customAdapter = new CustomAdapter(getApplicationContext(), restaurauntList);
                                    lv.setAdapter(customAdapter);
                                    //lv.invalidate();
                                    //customAdapter.notifyDataSetChanged();
                                }

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(FeedReaderContract.FeedEntry.KEY_CD_LOG_TAG, "Error JSON :" + e.toString());
                    }


                }
            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Failed to load", Toast.LENGTH_SHORT).show();
                        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
                        progressBar.setVisibility(View.GONE);
                        TextView tve = (TextView) findViewById(R.id.textViewempty);
                        tve.setVisibility(View.VISIBLE);
                    }
                });
            }
        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            int status = NetworkUtil.getConnectivityStatus(getApplicationContext());
            if (status != 0) {
                boolean active = NetworkUtil.hasActiveInternetConnection(getApplicationContext());
                if (active) {
                    UserFunctions userFunctions = new UserFunctions();
                    JSONObject json = userFunctions.getRestaurantDetails();
                    if (json == null || json.equals("") || !json.has(FeedReaderContract.FeedEntry.KEY_JSON_TAG_STATUS)) {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Failed toload,Loading from cache", Toast.LENGTH_SHORT).show();
                                }
                            });
                            DatabaseHandler dbDatabaseHandler = new DatabaseHandler(getApplicationContext());
                            JSONObject offlinejson = new JSONObject(dbDatabaseHandler.fetchFromCache("task"));
                            return offlinejson;
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(FeedReaderContract.FeedEntry.KEY_CD_LOG_TAG, "JSON EXCEPTION1 " + e.toString());
                            return null;
                        }
                    }
                    return json;

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "No connectivity,Loading from cache", Toast.LENGTH_SHORT).show();
                        }
                    });

                    try {
                        JSONObject offlinejson;
                        DatabaseHandler dbDatabaseHandler = new DatabaseHandler(getApplicationContext());
                        String cjson=dbDatabaseHandler.fetchFromCache("task");
                        if(cjson!=null||!cjson.equals("")){
                         offlinejson = new JSONObject(cjson);
                        }else{
                          offlinejson=null;
                        }
                        return offlinejson;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(FeedReaderContract.FeedEntry.KEY_CD_LOG_TAG, "JSON EXCEPTION2 " + e.toString());
                        return null;
                    }

                }

            } else {
                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "No Connectivity,Loading from cache", Toast.LENGTH_SHORT).show();
                        }
                    });
                    DatabaseHandler dbDatabaseHandler = new DatabaseHandler(getApplicationContext());
                    JSONObject offlinejson = new JSONObject(dbDatabaseHandler.fetchFromCache("task"));
                    return offlinejson;
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(FeedReaderContract.FeedEntry.KEY_CD_LOG_TAG, "JSON EXCEPTION3 " + e.toString());
                    return null;
                }

            }
        }
    }

    public void getLocation() {
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                uLat = location.getLatitude();
                uLon = location.getLongitude();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                // handle Location provider when disabled.
                Toast.makeText(getApplicationContext(),"Enable Location services",Toast.LENGTH_LONG).show();
            }
        };

        // for low battery usage Coarse loacation is used using Network provider.
        // if the we want to change the appox. location to Fine then we have to use GPS provider radio but it consumes more battery power.
        //
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5000, locationListener);

    }

    public double calDistance(double uLat, double uLon, double tLat, double tLon) {


        Location user = new Location("user");
        user.setLatitude(uLat);
        user.setLongitude(uLon);
        Location destination = new Location("destination");
        destination.setLatitude(tLat);
        destination.setLongitude(tLon);

        double distance = user.distanceTo(destination);


        return distance;
    }


}
